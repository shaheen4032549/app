**Task 1: Creating the folder structure**

Given a JSON tree with leaf nodes as an array. Represent it in the form of a folder structure where upper nodes act as folders and leaf nodes act as files.

There should be a clear distinction between the folders and files, and the interaction on folders should work properly (i.e. clicking on arrow should hide/show the child folders and files)

**Each subfolder should be indented accordingly to its depth in the tree, a user should be able to Create/Edit/Delete a file or folder to the user's desired location.**
Screenshots:

![Alt text](<Screenshot (53).png>)

![Alt text](<Screenshot (54).png>)

![Alt text](<Screenshot (55).png>)

![Alt text](<Screenshot (56).png>)

Video :

<video src="React%20App%20-%20Google%20Chrome%202023-12-09%2023-18-35.mp4" controls title="Title"></video>


Deployment Link
https://stunning-faun-bc6d52.netlify.app/



SECTION C:
1. Write and share a small note about your choice of system to schedule periodic tasks (such as downloading list of ISINs every 24 hours). Why did you choose it? Is it reliable enough; Or will it scale? If not, what are the problems with it? And, what else would you recommend to fix this problem at scale in production?

Cron stands out as the optimal tool for scheduling periodic tasks, such as the crucial activity of downloading lists of ISINs every 24 hours, particularly when dealing with sensitive personal information that constantly faces the threat of potential leaks. Its attributes provide a high level of assurance against such risks.

First and foremost, cron is highly reliable, specifically designed for Unix-like operating systems to schedule tasks based on time. It has undergone extensive testing in diverse environments, proving its robustness and dependability under various conditions and across multiple users.

Additionally, cron boasts simplicity in its usage. The tool is user-friendly, allowing for the smooth execution of assigned tasks without encountering obstacles. Its streamlined functionality not only accelerates the process but also enhances decision-making by ensuring a straightforward and efficient approach to task scheduling.


2. Suppose you are building a financial planning tool - which requires us to fetch bank statements from the user. Now, we would like to encrypt the data - so that nobody - not even the developer can view it. What would you do to solve this problem?

In the development of a financial planning tool requiring access to user bank statements, prioritizing security is crucial. To protect sensitive data, encryption becomes indispensable, ensuring that even the developer cannot view it. Several security measures must be implemented:

1. Utilize HTTPS for a more secure environment.
2. Introduce end-to-end encryption to shield user data from unauthorized access.
3. Encrypt user data stored on the bank server to add an extra layer of security.
4. Safeguard the security key, keeping it inaccessible to unauthorized individuals.
5. Implement a stringent password policy, creating strong and difficult-to-crack login credentials.
6. Incorporate multi-level authentication methods to enhance overall security.
7. Conduct regular audits to identify and address potential risks, staying vigilant against emerging security threats, and keeping security systems up-to-date.
8. Follow a data minimization strategy, retaining only precise and essential information to mitigate the risk of data breaches.
9. Ensure compliance with data protection regulations, tailoring the approach based on the nature of the stored data.
10. Educate users about potential security threats, emphasizing the significance of strong passwords and discouraging the sharing of credentials.
11. Maintain a continuous focus on robust cybersecurity practices, regularly evaluating and modifying systems to enhance security measures.